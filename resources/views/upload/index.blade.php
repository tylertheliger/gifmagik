@extends('app')

@section('content')
    @parent
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Upload Gif</h3>
        </div>
        <div class="panel-body">
            <form action="{{route('upload.store')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="gif" class="form-control">Upload a Gif</label>
                    <input required type="file" name="gif" class="form-control">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-lg btn-default" value="upload">
                </div>
            </form>
          </div>
    </div>
@endsection
