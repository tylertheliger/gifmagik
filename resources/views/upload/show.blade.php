@extends('app')

@section('content')
    @parent
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Modify Gif</h3>
        </div>
        <div class="panel-body">
            <form>
                <div class="row">
                    @foreach($gif->frames()->get() as $frame)
                        <div class="col-lg-4">
                            <canvas id="{{$frame->id}}" width="320px" height="270px" class="frame_canvas"></canvas>
                            <img id="image_{{$frame->id}}" class="hidden" src="/images/{{$frame->gif->name}}/{{$frame->name}}"/>
                            <input id="frame_coords_{{$frame->id}}" type="hidden" name="frame_coords[{{$frame->id}}]"/>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="form-group">
                        <input type="submit" class="btn btn-lg btn-default" value="Next"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">

        function getMousePos(canvas, evt) {
            var rect = canvas.getBoundingClientRect();
            return {
                x: evt.clientX - rect.left,
                y: evt.clientY - rect.top
            };
        }

        var canvas_obj =  {
            onReady: function() {
                var frame_canvas = $('.frame_canvas');
                frame_canvas.each(function(i) {
                    var context = this.getContext('2d');
                    var imageObj = new Image();
                    var frame_id = $(this).attr('id');
                    var related_image = $('#image_'+frame_id);

                    imageObj.onload = function() {
                        context.drawImage(imageObj,0, 10);
                    };
                    imageObj.src = related_image.attr('src');

                    this.addEventListener('click', function(evt) {
                        frame_id = $(this).attr('id');
                        var mousePos = getMousePos(this, evt);
                        var coords  = mousePos.x + ',' + mousePos.y;
                        context.fillStyle = "#FF0000";
                        context.beginPath();
                        context.arc(mousePos.x, mousePos.y, 25, 0, 2*Math.PI);
                        context.fill();
                        $('#frame_coords_'+frame_id).val(coords);
                    }, false);
                })
            },
        };

        $(document).ready(canvas_obj.onReady);

    </script>
@endsection
