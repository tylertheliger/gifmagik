<?php

namespace App\Http\Controllers;

use App\Handler\FileType\GifFileType;
use App\Handler\FileType\ImageFileType;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Gif;
use Illuminate\Support\Facades\Storage;
use App\Frame;
use Illuminate\Support\Facades\Input;
use App\Image;
use Intervention\Image\ImageManagerStatic as InvImage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class GifUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('upload/index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file_uploader = app('FileUploadHandler');
        $file_uploader->save($request->file('gif'), new GifFileType());
        $gif = Gif::create();
        $gif->name = $file_uploader->getFileName();
        $gif->path = $file_uploader->getFileDir();
        $gif->save();

        $frames = $file_uploader->getFrames();
        $frame_array = array();

        foreach( $frames as $frame) {
            $frame_obj = new Frame();
            $frame_obj->name = $frame;
            $frame_obj->gif()->associate($gif);
            $frame_obj->save();
            $gif->frames()->save($frame_obj);
        }

        $gif->save();

        return redirect()->action('GifUploadController@uploadStepTwo', array('id' => $gif->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gif = Gif::findOrFail($id);

        //$gifs = Storage::files('gifs/'.$original_gif->name.'/frames');

        return view('upload/show', array(
            'gif' => $gif
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * gets frame coordinates
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function uploadStepTwo($id)
    {
        $gif = Gif::findOrFail($id);

        return view('upload/upload_step_two', array(
            'gif' => $gif
        ));
    }

    /**
     * stores frame coordinates
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeStepTwo($id)
    {
        $gif = Gif::findOrFail($id);
        $frame_coord_array = Input::get('frame_coords', array());

        foreach ($gif->frames()->get() as $frame) {
            foreach ($frame_coord_array as $frame_id => $coords) {
                if($frame->id == $frame_id) {
                    if (!empty($coords)) {
                        $coord_split = explode(',',$coords);
                        $x_coord = $coord_split[0];
                        $y_coord = $coord_split[1];
                        $frame->x_coords = (int) $x_coord;
                        $frame->y_coords = (int) $y_coord;
                        $frame->save();
                    }
                }
            }
        }

        return redirect()->action('GifUploadController@uploadStepThree', array('id' => $id));
    }

    /**
     * Get the view to upload the user photo
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function uploadStepThree($id)
    {
        $gif = Gif::findOrFail($id);

        //$gifs = Storage::files('gifs/'.$original_gif->name.'/frames');

        return view('upload/upload_step_three', array(
            'gif' => $gif
        ));
    }

    /**
     * Stores the new image and converts/resizes it
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeStepThree($id, Request $request)
    {
        $gif = Gif::findOrNew($id);
        $image = $request->file('image');
        $alt_image = InvImage::make($image)->resize(320,280)->save('/tmp/original.png');
        $file = new UploadedFile($alt_image->dirname.'/'.$alt_image->basename, 'original.png');
        $file_uploader = app('FileUploadHandler');
        $file_uploader->setFileDir($gif->path);
        $file_uploader->setFileName('original.png');
        $file_uploader->save($file, new ImageFileType());
        $image = new Image();
        $image->name = $file_uploader->getFilename();
        $image->path = $file_uploader->getFileDir();
        $image->is_original = true;
        $image->gif()->associate($gif);
        $gif->images()->save($image);

        return redirect()->action('GifUploadController@uploadStepFour', array('id' => $id));
    }

    /**
     * Shows our image view
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function uploadStepFour($id)
    {
        $gif = Gif::findOrFail($id);
        return view('upload/upload_step_four', array (
            'gif' => $gif
        ));
    }

    public function storeStepFour($id, Request $request) {
        $gif = Gif::findOrFail($id);
        $request_image = $request->get('modified_image', array());
        $modified = InvImage::make($request_image)->resize(320,280)->save('/tmp/modified.png');

        $file = new UploadedFile($modified->dirname.'/'.$modified->basename, 'modified.png');
        $file_uploader = app('FileUploadHandler');
        $file_uploader->setFileName('modified.png');
        $file_uploader->setFileDir($gif->path);
        $file_uploader->save($file, new ImageFileType());
        $image = new Image();

        $image->name = $file_uploader->getFilename();
        $image->path = $file_uploader->getFileDir();
        $image->is_original = false;
        $image->gif()->associate($gif);
        $gif->images()->save($image);

        return redirect()->action('GifUploadController@uploadStepFive', array('id' => $id));

    }

    public function uploadStepFive($id)
    {
        $gif = Gif::findOrFail($id);
        $image = Image::where('gif_id', '=', $id)
            ->where('is_original', '=', 'false')
            ->first();

        return view('upload/upload_step_five', array(
            'gif' => $gif,
            'image' => $image
        ));
    }

    public function storeStepFive($id, Request $request)
    {

        $x = $request->get('x', null);
        $y = $request->get('y', null);
        $w =  $request->get('w', null);
        $h = $request->get('h', null);

        $gif = Gif::findOrFail($id);
        $image = Image::where('gif_id', '=', $id)
            ->where('name', '=', 'modified.png')
            ->first();

        $file_image = Storage::get('images/'.$image->path.'/'.$image->name);
        $new_image = InvImage::make($file_image)->encode('png');
        $new_image->crop($w, $h, $x, $y);
        $new_image->save('/tmp/cropped.png');
        $file = new UploadedFile($new_image->dirname.'/'.$new_image->basename, 'cropped.png');

        $file_uploader = app('FileUploadHandler');
        $file_uploader->setFileDir($gif->path);
        $file_uploader->setFileName('cropped.png');
        $file_uploader->save($file, new ImageFileType());

        $image = new Image();
        $image->name = $file_uploader->getFilename();
        $image->path = $file_uploader->getFileDir();
        $image->is_original = true;
        $image->gif()->associate($gif);
        $gif->images()->save($image);

        return redirect()->action('GifUploadController@uploadStepSix', array('id' => $id));

    }

    public function uploadStepSix($id)
    {
        $gif = Gif::findOrFail($id);
        $image = Image::where('gif_id', '=', $id)
            ->where('name', '=', 'cropped.png')
            ->first();

        return view('upload/upload_step_six', array(
            'gif' => $gif,
            'image' => $image
        ));
    }

    public function storeStepSix($id, Request $request)
    {
        $gif = Gif::findOrFail($id);
        $image_width = $request->get('width' , 0);
        $image_height = $request->get('height', 0);
        $image = Storage::get('images/'.$gif->path.'/cropped.png');

        $new_image = InvImage::make($image)->resize($image_width, $image_height)->encode('png')->save('/tmp/resized.png');

        foreach ($gif->frames()->get() as $frame) {
            $file_frame = Storage::get('gifs/'.$gif->path.'/'.$frame->name);

            $frame_image = InvImage::make($file_frame)->encode('gif');
            $frame_image->insert('/tmp/resized.png', null, round($frame->x_coords/1.3), round($frame->y_coords/2));
            $frame_image->save('/tmp/frame.gif');

            //save file to storage
            Storage::put(
                '/gifs/'.$gif->path . '/' . $frame->name,
                file_get_contents('/tmp/frame.gif')
            );
        }

        $joiner = app('FrameJoiner');
        $joiner->join($gif->path);

        return redirect()->action('GifUploadController@uploadFinalize', array('id' => $id));
    }

    public function uploadFinalize($id)
    {
        $gif = Gif::findOrFail($id);

        return view('upload/finalize', array (
            'gif' => $gif
        ));
    }
}
