<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGifTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifs', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default(null);
            $table->string('alternate_name')->default(null);
            $table->string('path')->default(null);
            $table->string('url')->default(null);
            $table->boolean('is_complete')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema:drop('gifs');       //drops the gifs table
    }
}
