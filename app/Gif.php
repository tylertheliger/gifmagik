<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gif extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gifs';

    public function frames()
    {
        return $this->hasMany('App\Frame');
    }

    public function images()
    {
        return $this->hasMany('App\Image');
    }
}
