<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 11/11/15
 * Time: 7:38 AM
 */

namespace App\Handler\FileType;


class FileType implements FileTypeInterface
{
    protected $base_dir;
    protected $is_splittable;

    public function getBaseDir()
    {
        return $this->base_dir;
    }

    public function setBaseDir($base_dir)
    {
        $this->base_dir = $base_dir;

        return $this;
    }

    public function getIsSplittable()
    {
        return $this->is_splittable;
    }

    public function setIsSplittable($is_splittable)
    {
        $this->is_splittable = $is_splittable;

        return $this;
    }
}