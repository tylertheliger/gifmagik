<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 11/11/15
 * Time: 7:38 AM
 */

namespace App\Handler\FileType;


class GifFileType extends FileType implements FileTypeInterface
{
    protected $base_dir = 'gifs/';
    protected $is_splittable = true;


}