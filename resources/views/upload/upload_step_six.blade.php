@extends('app')
@section('stylesheets')
    @parent
    <style>
    </style>
@endsection

@section('content')
    @parent
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">ReSize Image</h3>
        </div>
        <div class="panel-body">
            <form id="step_6_form" action="/upload/{{$gif->id}}/store_step_6" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4">
                        <div id="draggable" style="display:inline-block">
                            <img id="resize_image" src="/images/{{$image->path}}/{{$image->name}}" style="z-index:100"/>
                        </div>
                    </div>
                    <div class="col-md-4" style="z-index:1">
                        <img id="reference_frame" src="/gifs/{{$gif->path}}/frame_000.png">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group pull-right">
                        <div class="col-md-2 offset-2">
                            <input id="width" type="hidden" name="width" value="150">
                            <input id="height" type="hidden" name="height" value="125">
                            <input type="submit" class="btn btn-lg btn-default" value="ZUG ZUG"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        var reposition = '';
        $(function(){
            $("#draggable").draggable();
            $( "#resize_image" ).resizable();

            $('#step_6_form').on("submit", function(e) {
                e.preventDefault();
                var img = document.getElementById('resize_image');
                var width = img.clientWidth;
                var height = img.clientHeight;

                $('#width').val(width);
                $('#height').val(height);
                this.submit();
            });
        });
    </script>
@endsection
