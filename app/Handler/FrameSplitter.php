<?php

namespace App\Handler;

use GIFEndec\Frame;
use GIFEndec\Decoder;
use GIFEndec\MemoryStream;

class FrameSplitter
{
    protected $frames = array();

    public function split($storage_path)
    {
        $this->dir = $storage_path;

        /**
         * Load GIF to MemoryStream
         */
        $gifStream = new MemoryStream();
        $gifStream->loadFromFile(storage_path().'/app/'.$storage_path.'/original.gif');

        /**
         * Create Decoder instance from MemoryStream
         */
        $gifDecoder = new Decoder($gifStream);

        /**
         * Run decoder. Pass callback function to process decoded Frames when they're ready.
         */
        $gifDecoder->decode(function (Frame $frame, $index) {

            /**
             * Convert frame index to zero-padded strings (001, 002, 003)
             */
            $paddedIndex = str_pad($index, 3, '0', STR_PAD_LEFT);

            $this->frames[] = 'frame_'.$paddedIndex.'.png';

            /**
             * Write frame images to directory
             */
            $frame->getStream()->copyContentsToFile(
                storage_path().'/app/'.$this->dir.'/frame_'.$paddedIndex.'.png'
            );
        });

        return $this->frames;
    }
}