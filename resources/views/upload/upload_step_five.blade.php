@extends('app')

@section('content')
    @parent
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Crop Image</h3>
        </div>
        <div class="panel-body">
            <form id="step_5_form" action="/upload/{{$gif->id}}/store_step_5" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                        <div class="col-md-6">
                            <label>{{$image->name}}</label>
                                <img id="crop_image" width="320px" height="240px" src="/images/{{$image->path}}/{{$image->name}}"/>
                        </div>
                        <div class="col-md-6">
                            <img id="preview"/>
                        </div>
                </div>
                <div class="row">
                    <div class="form-group pull-right">
                        <div class="col-md-2 offset-2">
                            <input id="x" type="hidden" name="x"/>
                            <input id="w" type="hidden" name="w"/>
                            <input id="y" type="hidden" name="y"/>
                            <input id="h" type="hidden" name="h"/>
                            <input id="x2" type="hidden" name="x2"/>
                            <input id="y2" type="hidden" name="y2"/>
                            <input type="submit" class="btn btn-lg btn-default" value="Ready to Work"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">

        $(function(){
            // for sample 1
            $('#crop_image').Jcrop({ // we linking Jcrop to our image with id=cropbox1
                aspectRatio: 0,
                onChange: updateCoords,
                onSelect: updateCoords
            });

            var i, ac;

            // A handler to kill the action
            function nothing(e) {
                e.stopPropagation();
                e.preventDefault();
                return false;
            };

            // Returns event handler for animation callback
            function anim_handler(ac) {
                return function(e) {
                    api.animateTo(ac);
                    return nothing(e);
                };
            };

            // Setup sample coordinates for animation
            var ac = {
                anim1: [0,0,40,600],
                anim2: [115,100,210,215],
                anim3: [80,10,760,585],
                anim4: [105,215,665,575],
                anim5: [495,150,570,235]
            };

            // Attach respective event handlers
            for(i in ac) jQuery('#'+i).click(anim_handler(ac[i]));

        });

        function updateCoords(c) {
            $('#x').val(c.x);
            $('#y').val(c.y);
            $('#w').val(c.w);
            $('#h').val(c.h);

            $('#x2').val(c.x2);
            $('#y2').val(c.y2);

            var rx = 200 / c.w; // 200 - preview box size
            var ry = 200 / c.h;

            /*
            $('#preview').css({
                width: Math.round(rx * 800) + 'px',
                height: Math.round(ry * 600) + 'px',
                marginLeft: '-' + Math.round(rx * c.x) + 'px',
                marginTop: '-' + Math.round(ry * c.y) + 'px'
            });
            */
        };

        function checkCoords() {
            if (parseInt($('#w').val())) return true;
            alert('Please select a crop region then press submit.');
            return false;
        };

    </script>
@endsection
