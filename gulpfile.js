var elixir = require('laravel-elixir');

var paths = {
    'jquery':    './bower_components/jquery/',
    'bootstrap': './bower_components/bootstrap-sass/assets/',
    'fabric':    './bower_components/fabric.js/',
    'jcrop':     './bower_components/Jcrop/',
    'jqueryui': './bower_components/jquery-ui/'
};

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss', 'public/css/', {
            includePaths: [
                paths.bootstrap + 'stylesheets/',
                paths.jcrop + 'css/jcrop.min.css'
            ]
        })
        .copy(paths.bootstrap + 'fonts/bootstrap/**', 'public/fonts')
        .copy(paths.jquery + 'dist/jquery.js', 'public/js')
        .copy(paths.bootstrap + 'javascripts/bootstrap.min.js', 'public/js')
        .copy(paths.fabric + 'dist/fabric.min.js', 'public/js')
        .copy(paths.jcrop + 'js/Jcrop.min.js', 'public/js')
        .copy(paths.jcrop + 'css/Jcrop.min.css', 'public/css')
        .copy(paths.jqueryui + '/jquery-ui.min.js', 'public/js')
        .copy(paths.jqueryui + '/ui/minified/resizable.min.js', 'public/js')
        .copy(paths.jqueryui + '/themes/ui-lightness/jquery-ui.min.css', 'public/css');
});