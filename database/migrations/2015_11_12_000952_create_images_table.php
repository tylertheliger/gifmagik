<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('gif_id')->unsigned();
            $table->string('name')->default(null);
            $table->boolean('is_complete')->default(false);
            $table->boolean('is_original')->default(false);
            $table->timestamps();

            $table->foreign('gif_id')->references('id')->on('gifs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('images');
    }
}
