@extends('app')

@section('content')
    @parent
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Upload Image</h3>
        </div>
        <div class="panel-body">
            <form action="/upload/{{$gif->id}}/store_step_3" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-6 offset-2">
                        <div class="form-group">
                            <label for="image">Upload an Image</label>
                            <input type="file" REQUIRED name="image" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group pull-right">
                        <div class="col-md-2 offset-2">
                            <input type="submit" class="btn btn-lg btn-default" value="Next"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">

        function getMousePos(canvas, evt) {
            var rect = canvas.getBoundingClientRect();
            return {
                x: evt.clientX - rect.left,
                y: evt.clientY - rect.top
            };
        }

        var canvas_obj =  {
            onReady: function() {
                var frame_canvas = $('.frame_canvas');
                frame_canvas.each(function(i) {
                    var context = this.getContext('2d');
                    var imageObj = new Image();
                    var frame_id = $(this).attr('id');
                    var related_image = $('#image_'+frame_id);

                    imageObj.onload = function() {
                        context.drawImage(imageObj,0, 10);
                    };
                    imageObj.src = related_image.attr('src');

                    this.addEventListener('click', function(evt) {
                        frame_id = $(this).attr('id');
                        var mousePos = getMousePos(this, evt);
                        var coords  = mousePos.x + ',' + mousePos.y;
                        context.fillStyle = "#FF0000";
                        context.beginPath();
                        context.arc(mousePos.x, mousePos.y, 25, 0, 2*Math.PI);
                        context.fill();
                        $('#frame_coords_'+frame_id).val(coords);
                    }, false);
                })
            },
        };

        $(document).ready(canvas_obj.onReady);

    </script>
@endsection
