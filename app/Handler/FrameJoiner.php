<?php
namespace app\Handler;

use GIFEndec\Color;
use GIFEndec\Encoder;
use GIFEndec\Frame;
use GIFEndec\MemoryStream;

class FrameJoiner
{
    public function join($path)
    {

        $gif = new Encoder();

        foreach (glob(storage_path().'/app/gifs/'.$path.'/frame*.png') as $file) {
            $stream = new MemoryStream();
            $stream->loadFromFile($file);
            $frame = new Frame();
            $frame->setDisposalMethod(1);
            $frame->setStream($stream);
            //$frame->setDuration(30); // 0.30s
            //$frame->setTransparentColor(new Color(255, 255, 255));
            $gif->addFrame($frame);
        }

        $gif->addFooter(); // Required after you're done with adding frames

// Copy result animation to file
        $gif->getStream()->copyContentsToFile(storage_path().'/app/gifs/'.$path.'/completed.gif');
    }
}