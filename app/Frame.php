<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frame extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'frames';

    protected $fillable = ['name', 'gif', 'x_coords', 'y_coords'];

    public function gif()
    {
        return $this->belongsTo('App\Gif', 'gif_id');
    }
}
