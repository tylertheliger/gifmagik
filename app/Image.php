<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images';

    protected $fillable = ['name', 'is_complete', 'is_original'];

    public function gif()
    {
        return $this->belongsTo('App\Gif', 'gif_id');
    }
}
