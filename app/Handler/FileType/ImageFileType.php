<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 11/11/15
 * Time: 7:38 AM
 */

namespace App\Handler\FileType;


class ImageFileType extends FileType implements FileTypeInterface
{
    protected $base_dir = 'images/';
    protected $is_splittable = false;

}