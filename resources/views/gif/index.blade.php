@extends('welcome')

@section('content')
    @parent
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Upload Gif</h3>
        </div>
        <div class="panel-body">
            <form>
                <div class="form-group">
                    <label for="gif" class="form-control">Gif</label>
                    <input type="file" name="gif" class="form-control">
                </div>
            </form>
          </div>
    </div>
@endsection
