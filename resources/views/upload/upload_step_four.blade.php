@extends('app')

@section('content')
    @parent
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Upload Image</h3>
        </div>
        <div class="panel-body">
            <form id="step_4_form" action="/upload/{{$gif->id}}/store_step_4" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                    @foreach($gif->images()->get() as $image)
                        <div class="col-md-6">
                            <label>Original:</label>
                            <canvas id="{{$image->id}}" width="320px" height="240px" class="image_canvas"></canvas>
                            <img id="image_{{$image->id}}" class="hidden" src="/images/{{$image->path}}/{{$image->name}}"/>
                            <input type="hidden" id="modified_image" name="modified_image" />
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="form-group pull-right">
                        <div class="col-md-2 offset-2">
                            <input type="submit" class="btn btn-lg btn-default" value="Ready to Work"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">

        var canvas_obj =  {
            onReady: function() {
                var canvas = $('.image_canvas');
                canvas.each(function(i) {
                    var ctx = this.getContext('2d');
                    var img = document.createElement('IMG');
                    var image_id = this.id;
                    var related_image = $('#image_'+image_id);

                    img.onload = function() {
                        var OwnCanv = new fabric.Canvas(image_id, {
                            isDrawingMode: true
                        });

                        var imgInstance = new fabric.Image(img, {
                            left: 0,
                            top: 0
                        });
                        OwnCanv.add(imgInstance);

                        OwnCanv.freeDrawingBrush.color = "transparent";
                        OwnCanv.freeDrawingBrush.width = 4;

                        OwnCanv.on('path:created', function(options) {
                            var path = options.path;
                            OwnCanv.isDrawingMode = false;
                            OwnCanv.remove(imgInstance);
                            OwnCanv.remove(path);
                            OwnCanv.clipTo = function(ctx) {
                                path.render(ctx);
                            };
                            OwnCanv.add(imgInstance);
                        });

                        //saves our modified base64 image to an input
                        OwnCanv.on('mouse:up', function(e) {
                            var img = this.toDataURL('image/png');
                            $("#modified_image").val(img);
                        });
                    };

                    img.src = related_image.attr('src');
                })
            }
        };

        var form_handler = {
            onSubmit: function(e) {
                e.preventDefault();
            }
        };

        $(document).ready(canvas_obj.onReady);
        $(document).click(canvas_obj.onMouseUp);
        //$(document).submit(form_handler.onSubmit);
    </script>
@endsection
