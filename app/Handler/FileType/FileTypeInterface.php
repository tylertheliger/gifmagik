<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 11/11/15
 * Time: 7:46 AM
 */

namespace App\Handler\FileType;


interface FileTypeInterface
{
    public function getIsSplittable();
    public function setIsSplittable($is_splittable);
    public function getBaseDir();
    public function setBaseDir($base_dir);
}