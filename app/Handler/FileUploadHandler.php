<?php

namespace App\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Handler\FrameSplitter;
use App\Handler\FileType\FileTypeInterface;

class FileUploadHandler
{
    protected $request;
    protected $file;
    protected $file_name = 'original.gif';
    protected $storage;
    protected $file_path;
    protected $frame_splitter;
    protected $frames;
    protected $file_dir;
    protected $frame_joiner;
    protected $is_joinable = false;


    function __construct(FrameSplitter $frame_splitter)
    {
        $this->frame_splitter = $frame_splitter;
    }

    public function getFilePath()
    {
        return $this->file_path;
    }

    public function setFilePath($file_path)
    {
        $this->file_path = $file_path;

        return $this;
    }

    public function setFileType(FileTypeInterface $file_type)
    {
        $this->file_type = $file_type;
    }

    public function getFileType()
    {
        return $this->file_type;
    }

    public function getFrameSplitter()
    {
        return $this->frame_splitter;
    }

    public function setFrameSplitter(FrameSplitter $frame_splitter)
    {
        $this->frame_splitter = $frame_splitter;

        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile(UploadedFile $file)
    {
        $this->file = $file;

        return $this;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }

    public function setFileDir($file_dir)
    {
        $this->file_dir = $file_dir;

        return $this;
    }

    public function getFileDir()
    {
        return $this->file_dir;
    }


    public function save(UploadedFile $file, FileTypeInterface $file_type)
    {
        $this->setFile($file);
        $this->setFileType($file_type);
        $this->file_dir = $this->getFileDir() ? $this->getFileDir() : $this->random_string();
        $this->file_name = $this->getFileName();
        $this->file_path = $this->getFileType()->getBaseDir() . $this->file_dir;

        //save file to storage
        Storage::put(
            $this->file_path . '/' . $this->file_name,
            file_get_contents($file->getRealPath())
        );

        //slice file into frames
        if ($file_type->getIsSplittable()) {
            $this->frames = $this->frame_splitter->split($this->file_path);
        }

        return true;
    }

    public function random_string($length = 8)
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    public function getFileName()
    {
        return $this->file_name;
    }

    public function setFileName($file_name)
    {
        $this->file_name = $file_name;

        return $this;
    }

    public function getFrames()
    {
        return $this->frames;
    }
}