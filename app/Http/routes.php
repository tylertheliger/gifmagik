<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('upload/{id}/step_2', 'GifUploadController@uploadStepTwo');
Route::post('upload/{id}/store_step_2', 'GifUploadController@storeStepTwo');
Route::get('upload/{id}/step_3', 'GifUploadController@uploadStepThree');
Route::post('upload/{id}/store_step_3', 'GifUploadController@storeStepThree');
Route::get('upload/{id}/step_4', 'GifUploadController@uploadStepFour');
Route::post('upload/{id}/store_step_4', 'GifUploadController@storeStepFour');
Route::get('upload/{id}/finalize', 'GifUploadController@uploadFinalize');
Route::post('upload/{id}/store_step_5', 'GifUploadController@storeStepFive');
Route::get('upload/{id}/step_5', 'GifUploadController@uploadStepFive');
Route::post('upload/{id}/store_step_6', 'GifUploadController@storeStepSix');
Route::get('upload/{id}/step_6', 'GifUploadController@uploadStepSix');
Route::resource('upload', 'GifUploadController');



Route::resource('/', 'GifController');

Route::get('gifs/{directory}/{filename}', function ($directory, $filename)
{
    $path = storage_path() . '/app/gifs/' . $directory . '/' .$filename;
    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('images/{directory}/{filename}', function ($directory, $filename)
{
    $path = storage_path() . '/app/images/' . $directory . '/' .$filename;
    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});


/**
 * Services
 */
$this->app->bind('FileUploadHandler', function ($app) {
    return new App\Handler\FileUploadHandler(
        new App\Handler\FrameSplitter()
    );
});

$this->app->bind('FrameJoiner', function($app) {
   return new App\Handler\FrameJoiner();
});